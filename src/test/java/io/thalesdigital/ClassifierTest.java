package io.thalesdigital;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClassifierTest {
  @BeforeEach
  void setup() throws IOException {
    GeographicalFileData db = new GeographicalFileData("src/test/resources/foobar.json");
    GeographicalRepositoryProvider.setInstance(db);
  }

  @Test
  void check_mig() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(48.7667, -3.05, 50), new Vector3D(0, 500, 0), "MIG");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("HIGH", threatLevel);

  }

  @Test
  void check_rafale() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(48.7667, -3.05, 50), new Vector3D(0, 500, 0), "Rafale");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("LOW", threatLevel);

  }

  @Test
  void check_a320() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(30, 20, 10), new Vector3D(0, 500, 0), "A-320");

    ArrayList<Area> area = new ArrayList<>();
    area.add(new Area(new Vector3D(30, 20, 10), 100));
    GeographicalFileData db = (GeographicalFileData) GeographicalRepositoryProvider.getInstance();
    db.setAllAreas(area);

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("HIGH", threatLevel);
  }

  @Test
  void check_a320_bis() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(10, 10, 50), new Vector3D(0, 500, 0), "A-320");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("LOW", threatLevel);
  }

  @Test
  void check_a320_radius() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(40, 20, 10), new Vector3D(0, 500, 0), "A-320");

    ArrayList<Area> area = new ArrayList<>();
    area.add(new Area(new Vector3D(30, 20, 10), 100));
    GeographicalFileData db = (GeographicalFileData) GeographicalRepositoryProvider.getInstance();
    db.setAllAreas(area);

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("HIGH", threatLevel);
  }

  @Test
  void check_f35() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(30, 20, 10), new Vector3D(0, 500, 0), "F-35");

    ArrayList<Area> area = new ArrayList<>();
    area.add(new Area(new Vector3D(30, 20, 10), 100));
    GeographicalFileData db = (GeographicalFileData) GeographicalRepositoryProvider.getInstance();
    db.setAllAreas(area);

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("HIGH", threatLevel);
  }

  @Test
  void check_f35_bis() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(10, 10, 50), new Vector3D(0, 500, 0), "A-320");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("LOW", threatLevel);
  }

  @Test
  void check_f35_medium() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(10, 10, 50), new Vector3D(0, 500, 0), "F-35");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("MEDIUM", threatLevel);
  }


  @Test
  void check_others() {
    Aircraft aircraft = new Aircraft(UUID.randomUUID(), new Vector3D(10, 10, 50), new Vector3D(0, 500, 0), "CESSNA");

    Classifier classifier = new Classifier();

    String threatLevel = classifier.getThreatLevelFor(aircraft);

    Assertions.assertEquals("MEDIUM", threatLevel);
  }
}
