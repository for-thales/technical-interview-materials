package io.thalesdigital;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class GeographicalFileData implements GeographicalRepository {

  private ArrayList<Area> areas = new ArrayList<>();
  private Vector3D ownLocation;
  private LocalTime refTime;

  public GeographicalFileData(String path) throws IOException {
    Path filename = Path.of(path);
    List<String> lines = Files.readAllLines(filename);
    String content = "";
    for (int i = 0; i < lines.size(); i++) {
      content += lines.get(i);
    }

    JSONObject jsonConfig = new JSONObject(content);

    JSONArray configArea = jsonConfig.getJSONArray("areas");
    for (int i = 0; i < configArea.length(); i++) {
      JSONObject obj = configArea.getJSONObject(i);
      int x = obj.getInt("x");
      int y = obj.getInt("y");
      int z = obj.getInt("z");
      int r = obj.getInt("radius");
      Vector3D vector = new Vector3D(x, y, z);
      Area area = new Area(vector, r);
      areas.add(area);
    }

    JSONObject configLocation = jsonConfig.getJSONObject("own_location");
    int x = configLocation.getInt("x");
    int y = configLocation.getInt("y");
    int z = configLocation.getInt("z");
    ownLocation = new Vector3D(x, y, z);

    JSONObject configTime = jsonConfig.getJSONObject("reference_time");
    int h = configTime.getInt("hour");
    int m = configTime.getInt("minutes");
    int s = configTime.getInt("second");
    int n = configTime.getInt("ms");
    refTime = LocalTime.of(h, m, s, n * 1000000);
  }

  @Override
  public List<Area> getAllAreas() {
    return areas;
  }

  @Override
  public void setAllAreas(ArrayList<Area> areas) {
    this.areas = areas;
  }

  @Override
  public Vector3D getOwnLocation() {
    return ownLocation;
  }

  @Override
  public void setOwnLocation(Vector3D loc) {
    this.ownLocation = loc;
  }

  @Override
  public LocalTime getReferenceTime() {
    return refTime;
  }

  @Override
  public void setReferenceTime(LocalTime time) {
    this.refTime = time;
  }
}
