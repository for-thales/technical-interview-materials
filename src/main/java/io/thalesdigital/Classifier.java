package io.thalesdigital;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Classifier {
  public String getThreatLevelFor(Aircraft aircraft) {
    GeographicalRepository database = GeographicalRepositoryProvider.getInstance();

    if (database == null) {
      return "";
    } else if (database instanceof SQLGeographicalDatabase) {
      try {
        ((SQLGeographicalDatabase) database).connect();
      } catch (DBConnectionException exception) {
        System.out.println("Cannot get areas: access to DB refused");
        return "";
      }
    }

    Vector3D ownLocation = database.getOwnLocation();
    List<Area> areasInSector = new ArrayList<>();
    for (Area area : database.getAllAreas()) {
      double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2) + Math.pow(area.center.y - ownLocation.y, 2));
      if (distance <= 1000) {
        areasInSector.add(area);
      }
    }

    return process(aircraft, areasInSector);
  }

  private static String process(Aircraft aircraft, List<Area> areas) {
    double norm = Math.sqrt(Math.pow(aircraft.speed.x, 2)
        + Math.pow(aircraft.speed.y, 2)
        + Math.pow(aircraft.speed.z, 2));
    if (norm > 2000)
      return "HIGH";

    if (aircraft.type.equals("MIG"))
      return "HIGH";
    if (aircraft.type.equals("Rafale"))
      return "LOW";
    if (aircraft.type.equals("A-320")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "LOW";
    }
    if (aircraft.type.equals("F-35") || aircraft.type.equals("UNKNOWN")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "MEDIUM";
    }

    return "MEDIUM";
  }

  private static boolean checkIfInArea(Vector3D position, List<Area> areas) {
    for (Area area : areas) {
      if (area.contains(position)) {
        return true;
      }
    }
    return false;
  }

}
