package io.thalesdigital;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLGeographicalDatabase implements GeographicalRepository {
  public List<Area> getAllAreas() {
    return Arrays.asList(
        new Area(new Vector3D(10, 10, 10), 5),
        new Area(new Vector3D(15, 15, 10), 10),
        new Area(new Vector3D(100, 100, 10), 50)
    );
  }

  @Override
  public void setAllAreas(ArrayList<Area> areas) {

  }

  public void connect() {

  }

  public Vector3D getOwnLocation() {
    return new Vector3D(50, 50, 0);
  }

  @Override
  public void setOwnLocation(Vector3D loc) {

  }

  @Override
  public LocalTime getReferenceTime() {
    return null;
  }

  @Override
  public void setReferenceTime(LocalTime time) {

  }
}
