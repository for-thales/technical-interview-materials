package io.thalesdigital;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

interface GeographicalRepository {
  List<Area> getAllAreas();
  void setAllAreas(ArrayList<Area> areas);

  Vector3D getOwnLocation();
  void setOwnLocation(Vector3D loc);

  LocalTime getReferenceTime();
  void setReferenceTime(LocalTime time);
}
