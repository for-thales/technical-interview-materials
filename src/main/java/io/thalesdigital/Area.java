package io.thalesdigital;

public class Area {
  public Vector3D center;
  public int radius;

  public Area(Vector3D center, int radius) {
    this.center = center;
    this.radius = radius;
  }

  public boolean contains(Vector3D position) {
    return Math.pow(position.x - center.x, 2.0) + Math.pow(position.y - center.y, 2.0) + Math.pow(position.z - center.z, 2.0) <= radius * radius;
  }
}
