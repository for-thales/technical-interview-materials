package io.thalesdigital;

public class GeographicalRepositoryProvider {

  private static GeographicalRepository db;

  public static GeographicalRepository getInstance() {
    return db;
  }

  public static void setInstance(GeographicalRepository db) {
    GeographicalRepositoryProvider.db = db;
  }
}
