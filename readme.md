# Threats classification



# Data

Un aéronef contient des données suivantes :
- uuid
- position
- altitude
- direction
- vitesse
- type
- timestamp de première détection


Zones d'exclusion :
- centre, rayon, altitude


type :
- A320 (civil, ?)
- Rafale (militaire, allié)
- Cessna (civil  ?)
- MiG (militaire, opposants)
- F35 (militaire, indéterminé)
- Unknown


Règles:
- Unknown dans une zone d'exclusion, quelle que soit la hauteur -> HIGH
- Unknown depuis 10 minutes -> HIGH
- (militaire, opposants) -> HIGH
- civil dans une zone d'exclusion -> HIGH
- (militaire, indéterminé) dans une zone d'exclusion -> HIGH
- (militaire, indéterminé) -> MEDIUM
- civil à 10 minutes d'une zone d'exclusion -> MEDIUM
